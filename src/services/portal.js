import http from '../config/http'



// POST
const sendMail = (data) => http.post(`/contact`, data)

// GET
const listProducts = () => http.get(`/menu`)
const listCarousel = () => http.get(`/carousel`)

export {
    listProducts,
    listCarousel, 
    sendMail
}