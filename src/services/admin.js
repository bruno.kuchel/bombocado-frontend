import http from '../config/http'



// POST
const addCategory = (data) => http.post(`/category`, data)
const addProduct = (data, config = {}) => http.post(`/product`, data, config)
const addBanner = (data, config = {}) => http.post(`/banner`, data, config)

// GET
const listCategory = () => http.get(`/category`)
const listProducts = () => http.get(`/product`)
const listBanner = () => http.get(`/banner`)

// GET by id
const showCategory = (id) => http.get(`/category/${id}`)
const showProduct = (id) => http.get(`/product/${id}`)
const showBanner = (id) => http.get(`/banner/${id}`)

// UPDATE
const updateCategory = (id, data) => http.patch(`/category/${id}`, data)
const updateProduct = (id, data) => http.patch(`/product/${id}`, data)
const updateBanner = (id, data) => http.patch(`/banner/${id}`, data)

// DELETE by Id
const deleteCategory = (id) => http.delete(`/category/${id}`)
const deleteProduct = (id) => http.delete(`/product/${id}`)
const deleteBanner = (id) => http.delete(`/banner/${id}`)



export {
    addCategory,
    addProduct,
    addBanner,
    listCategory,
    listProducts,
    listBanner,
    showCategory,
    showProduct,
    showBanner,
    updateCategory,
    updateProduct,
    updateBanner,
    deleteCategory,
    deleteProduct,
    deleteBanner
    
}