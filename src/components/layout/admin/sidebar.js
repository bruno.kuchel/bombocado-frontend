import React from 'react'
import { Link } from 'react-router-dom'
import styled from 'styled-components'


import { FaImage, FaTags } from 'react-icons/fa'
import { MdRestaurantMenu } from 'react-icons/md'
import { BiTachometer } from 'react-icons/bi'


const Menu = [
    {
        name: "DASHBOARD",
        icon: <BiTachometer/>,
        link: "/"
    },
    {
        name: "BANNER",
        icon: <FaImage/>,
        link: "/banner"
    },
    {
        name: "CATEGORÍAS",
        icon: <FaTags/>,
        link: "/categorias"
    },
    {
        name: "PRODUCTOS",
        icon: <MdRestaurantMenu/>,
        link: "/productos"
    },
  
]

const Sidebar = () => {
    return (
        <>
        <Lateral>


            <ul className="navbar-nav sidebar sidebar-dark accordion" id="accordionSidebar">
                {/* Sidebar - Brand */}
                <Link to={'/admin'} className="sidebar-brand d-flex align-items-center justify-content-center">
                    <div className="sidebar-brand-icon">
                        <i className="fas fa-laugh-wink" />
                    </div>
                    <div className="sidebar-brand-text mx-3">Bombocado</div>
                </Link>


                {Menu.map((item, i) => (                    
                    
                        <ItemMenu key={i}>
                            <li className="nav-item collapsed">
                        <Link to={'/admin' + item.link} className="nav-link">
                        <i className="fa-fw">{item.icon}</i>
                        <span>{item.name}</span>                        
                        </Link>
                        <hr className="sidebar-divider" />
                        </li>
                        </ItemMenu>                  
                    
                    
                ))}
                

            </ul>

        </Lateral>

        </>
    );
}

export default Sidebar


const Lateral = styled.div`
background-color: #db3934;
    a {
        text-decoration: none;               
    }
    

`
const ItemMenu = styled.div`
 
     .nav-link{
        padding: 0.1rem 1rem !important;       
         align-items: center;

         i {
             padding: 0.5rem;
         }       
         
     }
`
