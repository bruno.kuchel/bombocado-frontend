import React from 'react'

import styled from 'styled-components'



const TitlePage = ({ title, titleBtn }) => {
    return (
        <>
                         
                
                    <Title>                
                        <h3>{ title }</h3>                       
                    </Title>              
                
            
        </>
    )
}



export default TitlePage




const Title = styled.div`
display: flex;

    border-bottom: 2px solid #084c61;    
    font-size: 2em;
    color: #084c61 !important; 
    margin-bottom: 1.5rem;

`
