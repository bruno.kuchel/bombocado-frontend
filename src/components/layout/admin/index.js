import React from 'react'
import '../../../assets/styles/css/sb-admin-2.min.css'
import styled from 'styled-components'

import Sidebar from './sidebar'
import Header from './header'
const Layout = ({ children}) => {
    return (
        <div id="wrapper">
            <Sidebar/>
            <div id="content-wrapper" className="d-flex flex-column">
                <div id="content">
                    <Header />
                    <Content>                    
                        {children}
                    </Content>
                    
                </div>

            </div>
        </div>

    )
}

export default Layout

const Content = styled.div`
margin-bottom: 1.5rem;
margin-top: 1.5rem;
`
