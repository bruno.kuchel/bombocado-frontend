import React from 'react'
import { getUser, removeToken } from '../../../config/auth'
import history from '../../../config/history'

import { FaRegUserCircle, FaSignOutAlt } from 'react-icons/fa'
import styled from 'styled-components' 

const Header = () => {

    const logout = () => {
        removeToken()
        history.push('/')
    }


    return (
        <>
            <Nav className="navbar navbar-expand navbar-light bg-white static-top shadow">
            <NavUser><FaRegUserCircle className="icon" />
                        {getUser().name}
                        <button onClick={logout}><FaSignOutAlt /></button>
                    </NavUser>

            </Nav>
            
        </>
    )
}

export default Header

const Nav = styled.div`

    justify-content: flex-end;
    .dropdown {
        position: relative;
    }
   
`

const NavUser = styled.div`
    color: #084c61;
    text-align: right;
    font-weight: bold;
    .icon {
        margin-right: 0.3rem;
        
        
    }
    button {
        background: transparent;
        border: none;
        margin-left: 0.5rem;
        color: #084c61;
        :hover {
            color: #DB3934;
        }
    }
`
