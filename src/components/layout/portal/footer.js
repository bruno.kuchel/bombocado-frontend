import React from 'react'
import { Container, Row, Col, Card } from 'react-bootstrap'
import styled from 'styled-components'
import { FaWhatsapp, FaInstagram, FaRegCalendarAlt, FaRegClock } from 'react-icons/fa'
import { MdMailOutline, MdLocationOn } from 'react-icons/md' 
import { AiOutlineShop } from 'react-icons/ai'



export default () => {
    return (
        <Footer>
            <Container>
                <FooterInfo>

                    <Row>
                        <Col md={4} className="no-gutters">
                        <Card>
                            <Card.Body>
                                <Card.Title>HAZ TU PEDIDO</Card.Title>
                                <Card.Text>
                                    <FaWhatsapp className="card-icon"/>
                                    652121546
                                </Card.Text>
                                <Card.Text>
                                    <FaInstagram className="card-icon"/>@bombocado
                                </Card.Text>
                                <Card.Text>
                                    <MdMailOutline className="card-icon"/>info@bombocado
                                </Card.Text>
                            </Card.Body>
                        </Card>
                        </Col>
                        <Col md={4} className="no-gutters">
                        <Card>
                            <Card.Body>
                                <Card.Title>HORARIO</Card.Title>
                                <Card.Text>
                                <FaRegCalendarAlt className="card-icon"/>Lunes a viernes
                                </Card.Text>
                                <Card.Text>
                                    <FaRegClock className="card-icon"/>Mañanas: 9 a 14h
                                </Card.Text>
                                <Card.Text>
                                <FaRegClock className="card-icon"/>Tardes: 17:30 a 20:30h
                                </Card.Text>
                            </Card.Body>
                        </Card>
                        </Col>
                        <Col md={4} className="no-gutters">
                        <Card>
                            <Card.Body>
                                <Card.Title>DONDE ESTAMOS</Card.Title>
                                <Card.Text><AiOutlineShop className="card-icon"/>
                                Mercado de Vicálvaro - Local 14                
                            </Card.Text>
                            <Card.Text><MdLocationOn className="card-icon"/>C/ de San Cipriano, 11 - 28032 - Madrid</Card.Text>                                
                            </Card.Body>
                        </Card>
                            <div>
                            
                            </div>

                        </Col>
                    </Row>

                </FooterInfo>
                <Row>

                    
                    <FooterCopy>
                    <div>© Bombocado 2020 - todos los derechos reservados</div>
            </FooterCopy>
                </Row>
            </Container>
        </Footer>

    )
}


const Footer = styled.div`
flex-shrink: 0;
background: #db3934;
font-size: 1em;
color: #FFF

`



const FooterInfo = styled.div`

    .no-gutters{
        padding-left: 0px;
        padding-right: 0px;
    }    
    .card{
        border: none;
        background: #db3934;
        
        .card-title{
            text-align: left;        
        }

        .card-text{
            
            font-size: 1em;
        }

        .card-icon{
            font-size: 1.3em;
            margin-right: 10px;
        }
    }
    

`


const FooterCopy = styled.div`
    padding: 15px;
    width: 100%;
    text-align: left;
    border-top: 2px solid #FFF;
    margin-top: 20px;

    
`