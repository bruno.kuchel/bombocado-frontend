import React from 'react'
import styled from 'styled-components'
import { Nav, Navbar, Container } from 'react-bootstrap'
import { NavLink } from 'react-router-dom'


export default () => {

    const menu = [
        {
            name: "CARTA",
            key: "1",
            link: "/carta"
        },
        {
            name: "SERVICIOS",
            key: "2",
            link: "/servicios"
        },
        {
            name: "SOBRE",
            key: "3",
            link: "/sobre"
        },
        {
            name: "CONTACTO",
            key: "4",
            link: "/contacto"
        }
        
    ]
    return (
        <Header>
            <Container>
            <Navbar collapseOnSelect expand="lg" variant="dark">
                <NavLink to={'/'}>
                <Navbar.Brand>BOMBOCADO</Navbar.Brand>
                </NavLink>
  
  <Navbar.Toggle aria-controls="responsive-navbar-nav" />
  <Navbar.Collapse id="responsive-navbar-nav" className="justify-content-end">
    <Nav>
        {menu.map((item, i) => (
            <NavLink exact={true} to={item.link} key={i} >
                <Nav.Link as="div" eventKey={item.key}>{item.name}</Nav.Link>
            </NavLink>
            

        ))}
      
      
    </Nav>    
  </Navbar.Collapse>
</Navbar>
</Container>
        </Header>

    )
}

const Header = styled.div`
    background-color: #db3934;
    a {
        text-decoration: none;
               
    }

    .navbar{
        padding: 10px 0px 10px 0px;
    }

    .navbar-brand{
        color: #FFF;
        font-size: 1.5em;
    }
    
    .navbar-toggler{
        padding: 0px;
        border: none;

              
    }

    
    .navbar-toggler:focus{
        outline: none;
        border: none;        
    }
    

    .nav-link{
        font-size: 1.2em;     
                   

        :hover{
        color: #084c61 !important; 
        font-weight: bold;
        
        }            
    }  
    
    .navbar-nav .active .nav-link {
        color: #084c61;
        font-weight:bold;
    }

    .navbar-nav .nav-link{
        color: #FFF;
    }

    .navbar-toggler-icon {
        
        background-image: url("data:image/svg+xml;charset=utf8,%3Csvg viewBox='0 0 30 30' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath stroke='rgba(255,255,255, 1)' stroke-width='2' stroke-linecap='square' stroke-miterlimit='10' d='M4 8h24M4 16h24M4 24h24'/%3E%3C/svg%3E");
}

`
