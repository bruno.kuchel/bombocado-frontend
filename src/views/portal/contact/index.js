import React, { useState }from 'react'
import { Col, Container, Row, InputGroup, FormControl, Button, Spinner } from 'react-bootstrap'
import TopTitle from '../../../components/layout/portal/title'
import styled from 'styled-components'
import { FaWhatsapp, FaUser } from 'react-icons/fa'
import { MdMailOutline } from 'react-icons/md' 
import { sendMail } from '../../../services/portal'
import Swal from 'sweetalert2'


const Sobre = () => {

    const [form, setForm] = useState({})
    const [loading, setLoading] = useState(false)


    const handleChange = (attr) => {
        setForm({
            ...form,
            [attr.target.name] : attr.target.value            
        })
    }

    const isFormValid = () => form.name && form.email && form.message

    const submitForm = async () => {
        const message = (type, message) => Swal.fire({                     
            title: message,
            icon: type,
            showConfirmButton: false,
            timer: 2000
          })
      
        if (isFormValid()){
            setLoading(true)

        try { 
            await sendMail(form)
            message('success','Mensaje enviado con éxito')            
            setLoading(false)
            setForm({})   
            

        } catch (error) {
            message('error','Error al enviar el mensaje')
            setLoading(false)            
        }
        }        
    }

    const pressEnter = (event) => event.key === 'Enter' ? submitForm() : null

    return (
        <>
            
            <TopTitle title="CONTACTO"/> 
            <ContainerContact>
                    <Row className="mb-4">
                        <Col md={6} className="mb-3">
                            <ColumnInfo>
                            <WrapInfo>
                                    <p>Nuestro horario de atención al cliente es de lunes a viernes de 9 a 20h, por teléfono, Whatsapp, e-mail o formulario de contacto.</p>   
                                </WrapInfo>
                                <WrapInfo className="mb-2">
                                    <IconInfo><FaWhatsapp/></IconInfo>
                                    <TextInfo>652121546</TextInfo>   
                                </WrapInfo>
                                <WrapInfo className="mb-3">
                                    <IconInfo><MdMailOutline/></IconInfo>
                                    <TextInfo>info@bombocado</TextInfo>   
                                </WrapInfo>                             
                            </ColumnInfo>
                            
                        </Col>
                        <Col md={6}>
                            <ColumnForm>
                            <InputGroup className="mb-4">
                                                <InputGroup.Prepend>
                                                    <InputGroup.Text className="prependBg"><FaUser /></InputGroup.Text>
                                                </InputGroup.Prepend>
                                                <FormControl type="text" name="name" className="input" placeholder="Nombre" onChange={handleChange} value={form.name || ""} onKeyPress={pressEnter}/>
                                            </InputGroup>
                                            <InputGroup className="mb-4">
                                                <InputGroup.Prepend>
                                                    <InputGroup.Text className="prependBg"><MdMailOutline /></InputGroup.Text>
                                                </InputGroup.Prepend>
                                                <FormControl type="email" name="email" className="input" placeholder="E-mail" onChange={handleChange} value={form.email || ""} onKeyPress={pressEnter}/>
                                            </InputGroup>
                                            <InputGroup>
                                                
                                                <FormControl as="textarea" name="message" className="input mb-4" placeholder="Escribe un mensaje" onChange={handleChange} value={form.message || ""} onKeyPress={pressEnter} rows={4}/>
                                            
                                                <Button variant="dark" block className="mb-5" onClick={submitForm} disabled={!isFormValid()}>
                                                {loading ? (
                                                    <Spinner animation="border" size="sm" />
                                                ) : "ENVIAR"}
                                            </Button></InputGroup>

                            </ColumnForm>
                           
                        </Col>
                    </Row>
                   
                    
                </ContainerContact>     

            
            
           
        </>
    )
}



export default Sobre

const ContainerContact = styled(Container)`
margin-top: 2rem;
margin-bottom: 2rem;
`
const ColumnInfo = styled.div`

`
const WrapInfo = styled.div`
display: flex;
`

const IconInfo = styled.div`
font-size: 1.5rem;
color: #084c61;
margin-right: 0.5rem;
`
const TextInfo = styled.div`
font-size: 1.1rem;
color: #084c61;
padding-top: 0.5rem;
`
const ColumnForm = styled.div`
 .prependBg{
        background-color: #FFF;
        border-top: none;
        border-right: none;
        //border-bottom: 2px solid;
        border-left: none;
        border-radius: 0px;
        color: #084c61;
    }

    

.input{
    background-color: #FFF;
    border-top: none;
    border-right: none;
    //border-bottom: 2px solid #084c61;
    border-left: none;
    border-radius: 0px;
    
          
}

.btn {
    background-color: #084c61;
    border: none;
    border-radius: 0px;
    transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
    :hover {
        background-color: #006266;
    }
    :disabled {
        background-color: #708090;
        cursor: not-allowed;
    }

}


.icon{
    font-size: 1.2em;
    margin-right: 10px;
}
   

`