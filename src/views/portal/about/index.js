import React from 'react'
import { Col, Container, Row, Image } from 'react-bootstrap'
import TopTitle from '../../../components/layout/portal/title'
import styled from 'styled-components'

const Sobre = () => {
    return (
        <>
            
            <TopTitle title="SOBRE NOSOTROS"/> 
            <ContainerAbout>
                    <Row className="mb-4">
                        <Col md={6} className="mb-3">
                            <Image fluid src="https://www.freemovement.org.uk/wp-content/uploads/2017/11/24546619124_f5e8cda65f_k-e1511202731931.jpg"/>
                        </Col>
                        <Col md={6}>
                            
                            <p>
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                            </p>
                        </Col>
                    </Row>
                   
                    
                </ContainerAbout>     

            
            
           
        </>
    )
}



export default Sobre

const ContainerAbout = styled(Container)`
margin-top: 2rem;
margin-bottom: 2rem;
`



