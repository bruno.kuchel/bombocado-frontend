import React from 'react'
import { Col, Container, Row, Image } from 'react-bootstrap'
import TopTitle from '../../../components/layout/portal/title'
import styled from 'styled-components'

const Servicios = () => {
    return (
        <>
           
                <TopTitle title="SERVICIOS"/>   
                <ContainerInfos>
                    <Row className="mb-4">
                        <Col md={6} className="mb-3">
                            <Image fluid src="https://img.grouponcdn.com/deal/61Gc8LTfkMVd8Ym5mtDcQ/produxit_catering-700x420/v1/c700x420.jpg"/>
                        </Col>
                        <Col md={6}>
                            <h4>KIT CUMPLEAÑOS</h4>
                            <p>
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                            </p>
                        </Col>
                    </Row>
                    <Row className="mb-4">
                        <Col md={6} className="mb-3">
                        <Image fluid src="https://cocina-casera.com/wp-content/uploads/2019/03/dia-del-padre1.jpg"/>
                        </Col>
                        <Col md={6}>
                        
                        <h4>MENU PERSONALIZADO</h4>
                            <p>
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                            </p>
                        </Col>
                    </Row>
                    
                </ContainerInfos>       
                
           
        </>
    )
}



export default Servicios

const ContainerInfos = styled(Container)`
margin-top: 2rem;
margin-bottom: 2rem;
`


