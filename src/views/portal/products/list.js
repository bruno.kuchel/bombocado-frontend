import React, { useState, useEffect } from 'react'
import { Card, Container } from 'react-bootstrap'
import styled from 'styled-components'
import { listProducts } from '../../../services/portal'
import Loading from '../../../components/loading'

const ProductsList = (props) => {
    const [products, setProducts] = useState([])
    const [update, setUpdate] = useState(false)
    const [isLoading, setIsLoading] = useState(false)
       

    useEffect(() => {
        setUpdate(false)
        let get = async () => { 
            setIsLoading(true)
            const prd = await listProducts() 
            setProducts(prd.data)
            setIsLoading(false)
        }        
            get()       
        
        return () => get = () => {
            //cleanup
        }
    }, [update])

    
    
         


    return (
        <>
        {isLoading
        ? <Loading/>
        : <ProductContainer>
            <RowProduct>                
            {products.map((prd, i) => (
                <CardProduct  key={i}>
                <Img>
                <Card.Img variant="top" src={prd.photo} />
                </Img>
                <Card.Body>
                    <Card.Title>{prd.title}</Card.Title>
                    <Card.Text>
                  {prd.description}
                  </Card.Text>                 
                </Card.Body>
                <Price>{prd.price}€</Price>
                
               
              </CardProduct>
                   
                ))}
            </RowProduct>
            </ProductContainer>
        }
        </>
    )
}



export default ProductsList

const ProductContainer = styled(Container)`
margin-top: 2rem;
margin-bottom: 2rem;
`


const CardProduct = styled(Card)`
//width: 25rem;
//margin-bottom: 1.5rem;
border-radius: 0.7rem;
border: none;

.card-title{
   // white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    //margin: 0px;
    text-transform: uppercase;
    color: #DB3934;
    font-weight: bold;
    border-bottom: 2px solid;
    padding-bottom: 1rem;
    
}

.card-img-top{
   // max-height: 175px;
    //overflow: hidden;
    
}

`
const Price = styled.div`
    padding: 0rem 1.25rem 1.25rem;
    color: #084c61;
    font-size: 1.25rem;
    font-weight: bold;
    //text-align: right;
`

const RowProduct = styled.div`
margin-top: 1rem;
display: grid;
grid-gap: 1rem;
grid-template-columns: repeat(auto-fill,minmax(300px,1fr));

`

const Img = styled.div`
max-height: 200px;
overflow: hidden;
`