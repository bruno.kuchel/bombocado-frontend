import React from 'react'
import { Route } from 'react-router-dom'

import Layout from '../../components/layout/portal'

import About from './about'
import Contact from './contact'
import Home from './home'
import Products from './products'
import Services from './services'

export default (props) => {
    return (
        <Layout>
            <Route exact basename={props.match.path} path={props.match.path + '/'} component={Home}/>
            <Route exact basename={props.match.path} path={props.match.path + 'sobre'} component={About}/>
            <Route exact basename={props.match.path} path={props.match.path + 'contacto'} component={Contact}/>
            <Route exact basename={props.match.path} path={props.match.path + 'carta'} component={Products}/>
            <Route exact basename={props.match.path} path={props.match.path + 'servicios'} component={Services}/>
        </Layout>

    )
}