import React from 'react'
import { Row, Col, Container, Card } from 'react-bootstrap'
import styled from 'styled-components'

export default () => {
    return (
        <ContainerInfos>
            <Container>
                <Row>
                    <Col md={4}>
                        <Card>
                            <Card.Img variant="top" src="https://www.flaticon.es/svg/static/icons/svg/3080/3080950.svg" width="112" height="112"/>
                            <Card.Body>
                                <Card.Title>CARD TITLE</Card.Title>
                                <Card.Text>
                                    Some quick example text to build on the card title and make up the bulk of
                                    the card's content.
                                </Card.Text>
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col md={4}>
                    <Card>
                            <Card.Img variant="top" src="https://www.flaticon.es/svg/static/icons/svg/2558/2558269.svg" width="112" height="112"/>
                            <Card.Body>
                                <Card.Title>CARD TITLE</Card.Title>
                                <Card.Text>
                                    Some quick example text to build on the card title and make up the bulk of
                                    the card's content.
                                </Card.Text>
                            </Card.Body>
                        </Card>

                    </Col>
                    <Col md={4}>
                    <Card>
                            <Card.Img variant="top" src="https://www.flaticon.es/svg/static/icons/svg/1400/1400111.svg" width="112" height="112" />
                            <Card.Body>
                                <Card.Title>CARD TITLE</Card.Title>
                                <Card.Text>
                                    Some quick example text to build on the card title and make up the bulk of
                                    the card's content.
                                </Card.Text>
                            </Card.Body>
                        </Card>

                    </Col>
                </Row>
            </Container>


        </ContainerInfos>
    )
}

const ContainerInfos = styled.div`
    padding-top: 30px;
    padding-bottom: 30px;
    color: #084c61;
    background: #F8F8FA;
    
    .no-gutters{
        padding-left: 0px;
        padding-right: 0px;
    }    

    .card{
        background: #F8F8FA;
        margin-bottom: 1.25rem;
        border: none;
        border-radius: 0.7rem;
        .card-img-top {
            padding-top: 1.25rem;

        }
        
        .card-title{
            text-align: center;
            font-weight: bold;
        
        }
    
    }
    `