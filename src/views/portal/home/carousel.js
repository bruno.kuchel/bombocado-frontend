import React, { useState, useEffect } from 'react'
import { listCarousel } from '../../../services/portal'
import { Carousel, Container, Row, Col, Card } from 'react-bootstrap'
import styled from 'styled-components'
import Bgbanner from '../../../assets/img/banner-background.png'

export default () => {

    const [banner, setBanner] = useState([])

    //const [isLoading, setIsLoading] = useState(false)
       

    useEffect(() => {        
        let get = async () => { 
            //setIsLoading(true)
            const bnr = await listCarousel() 
            setBanner(bnr.data)
            //setIsLoading(false)
        }        
            get()       
        
        return () => get = () => {
            //cleanup
        }
    }, [])



    return (
        <Background fluid>

            <Row>
                <BgCol  md={1} xl={2} />
                <BgCol xs={12} md={10} xl={8}>
                    <StyledCarousel controls={false}>
                        {banner.map((bnr, i) => (
                            <Carousel.Item key={i} className="justify-content-center align-items-center">
                            <Row className=" py-5 justify-content-center align-items-center">
                                <Col md={6} xl={6}>
                                    <CarouselImage>
                                        <Card.Img src={bnr.photo}/>                                       
                                        
                                    </CarouselImage>
                                </Col>
                                <Col md={6} xl={6}>
                                    <CarouselText>
                                        <Card.Title>{bnr.title}</Card.Title>
                                        <Card.Text>
                                        {bnr.description}
                  </Card.Text>
                                    </CarouselText>
                                </Col>
                            </Row>
                        </Carousel.Item>

                        ))}
                    
                        
                    </StyledCarousel>
                </BgCol>
                <BgCol  md={1} xl={2} />
            </Row>
        </Background >
    )
}

const Background = styled(Container)`
overflow: hidden;
background-image: url(${Bgbanner});
padding: 0px;
background-size:cover   ;

`
const BgCol = styled(Col)`
//padding: 0px;
`
const StyledCarousel = styled(Carousel)`
min-height: 630px;

`

const CarouselImage = styled(Card)`
background: transparent;
border: none;
`
const CarouselText = styled(Card)`
background: transparent;
padding: 10px;
border: none;
.card-title{
    font-size: 2.5em;
    font-weight: 600;
    color: #084c61;
}
.card-text{
    font-size: 1.5em;
    color: #084c61;
}

`