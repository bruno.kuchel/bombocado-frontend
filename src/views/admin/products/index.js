import React, { useState } from 'react'
import TitlePage from '../../../components/layout/admin/title'
import ProductsList from './list'
import ProductsForm from './form'

import { Button, Container } from 'react-bootstrap'
import { IoMdAddCircleOutline, IoIosList } from 'react-icons/io'
import styled from 'styled-components'


const Products = () => {

    const [isForm, setIsForm] = useState(false)
    const [updateList, setUpdateList] = useState({})   
 

    const updateProduct = (prd) => {        
        setIsForm(true)
        setUpdateList(prd)    
    }

    const newProduct = () => {
        setUpdateList(false)
        setIsForm(!isForm)
    }


        
    return (
        <>
        <Container fluid>
            <TitlePage title="PRODUCTOS"/>
            
                <ButtonStyled onClick={() => newProduct()}>
                    {isForm 
                        ? <><IoIosList className="icon"/> LISTA </>
                        : <><IoMdAddCircleOutline className="icon"/> NUEVO </>
                    }
                </ButtonStyled>
                    {isForm 
                        ? <ProductsForm update={updateList} />
                        : <ProductsList updateProduct={updateProduct}/>
                    }                             
            
        </Container>
                 
           
        </>
    )
}

export default Products

const ButtonStyled = styled(Button)`
padding: 0.5rem;
.icon {
    font-size: 1.3em;
    margin-right: 0.3rem;
    margin-top: -0.2rem;
    
}
`