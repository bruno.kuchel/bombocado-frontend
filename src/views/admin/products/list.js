import React, { useState, useEffect } from 'react'
import { Card } from 'react-bootstrap'
import styled from 'styled-components'
import { listProducts, deleteProduct } from '../../../services/admin'
import Loading from '../../../components/loading'

import { BiTrash, BiEdit } from 'react-icons/bi'
import Swal from 'sweetalert2'

const ProductsList = (props) => {
    const [products, setProducts] = useState([])
    const [update, setUpdate] = useState(false)
    const [isLoading, setIsLoading] = useState(false)
       

    useEffect(() => {
        setUpdate(false)
        let get = async () => { 
            setIsLoading(true)
            const prd = await listProducts() 
            setProducts(prd.data)
            setIsLoading(false)
        }        
            get()       
        
        return () => get = () => {
            //cleanup
        }
    }, [update])

    const removeProduct = async (catg) => {
        const message = (type, message) => Swal.fire({
            toast: true,
            position: 'bottom',
            width: '20rem',
            icon: type || '',
            title: message || '',
            showConfirmButton: false,
            timer: 2500
        })
    
        Swal.fire({
          title: `¿Deseas eliminar el producto?`,
          showCancelButton: true,
          confirmButtonText: `SÍ`,
          cancelButtonText: `NO`,
        }).then((result) => {
          if (result.isConfirmed) {
            deleteProduct(catg._id)
              .then(() => {
                setUpdate(true)
                message('success', `Producto eliminado con éxito.`)
              })
              .catch(() => message('danger', `Ha ocurrido un error`))
          }
        })
      }
    
      const sortProduct = products.sort(function (a, b) {

        if (a.status> b.status) {
          return -1
        }
        if (a.status < b.status) {
          return 1
        }
    
        return 0
    
    
      })
    


    return (
        <>
        {isLoading
        ? <Loading/>
        : <RowProduct>                
            {sortProduct.map((prd, i) => (
                    <CardProduct key={i} >
                    <Card.Header>
                    <Card.Title>{prd.title}</Card.Title> 
                    </Card.Header>
                    <Card.Body >                                        
                   <Card.Img variant="top" src={prd.photo} className={prd.status === true ? "" : "img-inactive"} />
                   </Card.Body> 
                    <Card.Footer>
                    <Actions><BiTrash className="icon" onClick={() => removeProduct(prd)} /><BiEdit className="icon" onClick={() =>props.updateProduct(prd)}/></Actions>
                    </Card.Footer>
                    </CardProduct>
                ))}
            </RowProduct>
        }
        </>
    )
}



export default ProductsList

const CardProduct = styled(Card)`
//width: 25rem;
//margin-bottom: 1.5rem;

.card-body{
    
    padding:0.75rem;
    display: flex;
    align-items: center;
    flex-wrap: wrap;
}


.card-title{
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    margin: 0px;
    text-transform: uppercase;
    
}

.card-img-top{
    max-height: 175px;
    
}

.img-inactive{
    filter: grayscale(100%);
    opacity: 60%;

}
.card-footer {
    padding: 0.5rem;
    text-align: right;
}

.right{
    text-align: right;
}
`
const RowProduct = styled.div`
margin-top: 1rem;
display: grid;
grid-gap: 1rem;
grid-template-columns: repeat(auto-fill,minmax(240px,1fr));

`
const Actions = styled.div`
display: block;
color: #6c757d;
.icon {
    font-size: 1.25em;
    cursor: pointer;
    margin-left: 0.5em;
}

`