import React from 'react'
import { Card } from 'react-bootstrap'
import styled from 'styled-components'

import { BiTrash, BiEdit } from 'react-icons/bi'

const ListCategories = () => {
    return (
        <>
        <RowCategory>
                <CardCategory>
  <Card.Header as="h5">ENTRANTES</Card.Header>
  <Card.Body>
    <Card.Title>Special title treatment</Card.Title>
    <Card.Text>
      With supporting text below as a natural lead-in to additional content.
    </Card.Text>
    
  </Card.Body>
  <Card.Footer className="text-muted right" ><BiTrash className="icon"/><BiEdit className="icon"/></Card.Footer>
</CardCategory>
<CardCategory>
  <Card.Header as="h5">Featured</Card.Header>
  <Card.Body>
    <Card.Title>Special title treatment</Card.Title>
    <Card.Text>
      With supporting text below as a natural lead-in to additional content.
    </Card.Text>
    
  </Card.Body>
</CardCategory>
<CardCategory>
  <Card.Header as="h5">Featured</Card.Header>
  <Card.Body>
    <Card.Title>Special title treatment</Card.Title>
    <Card.Text>
      With supporting text below as a natural lead-in to additional content.
    </Card.Text>
    
  </Card.Body>
</CardCategory>
<CardCategory>
  <Card.Header as="h5">Featured</Card.Header>
  <Card.Body>
    <Card.Title>Special title treatment</Card.Title>
    <Card.Text>
      With supporting text below as a natural lead-in to additional content.
    </Card.Text>
    
  </Card.Body>
</CardCategory>
<CardCategory>
  <Card.Header as="h5">Featured</Card.Header>
  <Card.Body>
    <Card.Title>Special title treatment</Card.Title>
    <Card.Text>
      With supporting text below as a natural lead-in to additional content.
    </Card.Text>
    
  </Card.Body>
</CardCategory>
<CardCategory>
  <Card.Header as="h5">Featured</Card.Header>
  <Card.Body>
    <Card.Title>Special title treatment</Card.Title>
    <Card.Text>
      With supporting text below as a natural lead-in to additional content.
    </Card.Text>
    
  </Card.Body>
</CardCategory>

                
                </RowCategory>
            

            
            
           
        </>
    )
}



export default ListCategories

const CardCategory = styled(Card)`
width: 15rem;
margin: 1.5rem;

.icon {
    font-size: 1.2em;
    cursor: pointer;
    margin-left: 0.5em;
}

.right{
    text-align: right;
}
`
const RowCategory = styled.div`
display: flex;
flex-wrap: wrap;
flex-direction: row;
//padding-left: 15px;
//padding-right: 15px;
align-content: space-around;
//padding: 15px;
`