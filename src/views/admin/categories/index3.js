import React, { useState, useEffect } from 'react'
import TitlePage from '../../../components/layout/admin/title'
import CategoriesList from './list'
import FormCategories from './form'
import history from '../../../config/history'

import { Button, Modal, Container } from 'react-bootstrap'
import { IoMdAddCircleOutline } from 'react-icons/io'
import styled from 'styled-components'


const Categories = () => {

    const [update, setUpdate] = useState({})
    const[updateList, setUpdateList] = useState(false)
    
    useEffect(() => {
        
      }, [setUpdateList])
    

    const updateCategory = (catg) => {        
        handleShow()
        setUpdate(catg)    
    }

    const newCategory = () => {
        handleShow()
        setUpdate(false)        
    }

    const [show, setShow] = useState(false)
    const handleClose = () => {
        history.push('/admin/categorias')
        setShow(false)
        
        
    }
    const handleShow = () => setShow(true)

    return (
        <>
        <Container fluid>
            <TitlePage title="CATEGORÍAS"/>
            
                <ButtonNew onClick={newCategory}><IoMdAddCircleOutline className="icon"/>NUEVA</ButtonNew>
                <CategoriesList updateCategory={updateCategory}/>  
            
                       
                       
            <Modal
                        show={show}
                        onHide={handleClose}
                        backdrop="static"
                        keyboard={false}        
                        //centered={true}
                    >      
                            <Modal.Body>
                                <FormCategories update={update} close={setShow} updateList={updateList}/>
                            </Modal.Body>       
                    </Modal>
        </Container>
                 
           
        </>
    )
}

export default Categories

const ButtonNew = styled(Button)`
padding: 0.5rem;
.icon {
    font-size: 1.3em;
    margin-right: 0.3rem;
    
}
`

