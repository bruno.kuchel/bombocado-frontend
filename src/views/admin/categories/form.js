import React, { useState } from 'react'
import { addCategory, updateCategory } from '../../../services/admin'
import Swal from 'sweetalert2'

import { Row, Col, FormControl, Card, Button, Spinner} from 'react-bootstrap'
import styled from 'styled-components'

const FormCategories = (props) => {

    const [form, setForm] = useState({
        ...props.update
    })   
          
    const handleChange = (attr) => {
        const { name, value } = attr.target
            setForm({
            ...form,
            [name] : value            
            })        
    }
    const typeReq = (data) => Object.keys(props.update).length > 0 ? updateCategory(props.update._id, data) : addCategory(data)

    const isUpdate = Object.keys(props.update).length > 0

    const [loading, setLoading] = useState(false)
    
    const isFormValid = () => form.name && form.icon

    const submitCategory = async () => {
        const message = (type, message) => Swal.fire({                     
            title: message,
            icon: type,
            showConfirmButton: false,
            timer: 2000
          })
      
        if (isFormValid()){
            setLoading(true)

        try { 
            await typeReq(form)
            message('success',`Categoría ${isUpdate ? "actualizada" : "creada" } con éxito`)            
            setLoading(false)
            setForm({})
            
            
            
            

        } catch (error) {
            setLoading(false)            
        }
        }        
    }

    
    return (
        <>
        <RowForm>            
            <Col sm md={2} xl={4}/>
            <Col sm md={8} xl={4}>
        <CardForm>
            <Card.Body>
                <Card.Title className="mb-4">{isUpdate ? "EDITAR CATEGORÍA" : "NUEVA CATEGORÍA"}</Card.Title>                                      
                <FormControl type="text" name="name" className="input mb-3" placeholder="Nombre de la categoría" onChange={handleChange} value={form.name || ""}/>
                <FormControl type="text" name="icon" className="input mb-3" placeholder="Imagen de la categoría" onChange={handleChange} value={form.icon || ""}/>
                <Action>
                    <Button className="btn" variant="primary"  onClick={submitCategory} disabled={!isFormValid()}>
                        {loading ? (
                        <Spinner animation="border" size="sm" />
                        ) : "ENVIAR"}
                    </Button >
                    
                </Action>                                            
            </Card.Body>
        </CardForm>     
        </Col>
        <Col sm md={2} xl={4}/>        
        </RowForm>   
        
        </>
    )
}

export default FormCategories

const CardForm = styled(Card)`
    background-color: #FFF;
   // border: none;
    border-radius: 0px;

    .btn{
        margin-left: 0.6rem;
    }
`
const Action = styled.div`
display: flex;
justify-content: flex-end;

`
const RowForm = styled(Row)`
margin-top: 1rem;
`
