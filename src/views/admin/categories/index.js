import React, { useState } from 'react'
import TitlePage from '../../../components/layout/admin/title'
import CategoriesList from './list'
import FormCategories from './form'

import { Button, Container } from 'react-bootstrap'
import { IoMdAddCircleOutline, IoIosList } from 'react-icons/io'
import styled from 'styled-components'


const Categories = () => {

    const [isForm, setIsForm] = useState(false)
    const [updateList, setUpdateList] = useState({})   
 

    const updateCategory = (catg) => {        
        setIsForm(true)
        setUpdateList(catg)    
    }

    const newCategory = () => {
        setUpdateList(false)
        setIsForm(!isForm)
    }


        
    return (
        <>
        <Container fluid>
            <TitlePage title="CATEGORÍAS"/>
            
                <ButtonStyled onClick={() => newCategory()}>
                    {isForm 
                        ? <><IoIosList className="icon"/> LISTA </>
                        : <><IoMdAddCircleOutline className="icon"/> NUEVA </>
                    }
                </ButtonStyled>
                    {isForm 
                        ? <FormCategories update={updateList} />
                        : <CategoriesList updateCategory={updateCategory}/>
                    }        
            
                       
                       
            
        </Container>
                 
           
        </>
    )
}

export default Categories

const ButtonStyled = styled(Button)`
padding: 0.5rem;
.icon {
    font-size: 1.3em;
    margin-right: 0.3rem;
    margin-top: -0.2rem;
    
}
`