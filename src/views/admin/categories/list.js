import React, { useState, useEffect } from 'react'
import { Card } from 'react-bootstrap'
import styled from 'styled-components'
import { listCategory, deleteCategory } from '../../../services/admin'
import Loading from '../../../components/loading'

import { BiTrash, BiEdit } from 'react-icons/bi'
import Swal from 'sweetalert2'

const CategoriesList = (props) => {
    const [categories, setCategories] = useState([])
    const [update, setUpdate] = useState(false)
    const [isLoading, setIsLoading] = useState(false)
       

    useEffect(() => {
        setUpdate(false)
        let get = async () => { 
            setIsLoading(true)
            const ctg = await listCategory() 
            setCategories(ctg.data)
            setIsLoading(false)
        }        
            get()       
        
        return () => get = () => {
            
        }
    }, [update])

    const removeCategory = async (catg) => {
        const message = (type, message) => Swal.fire({
            toast: true,
            position: 'bottom',
            width: '20rem',
            icon: type || '',
            title: message || '',
            showConfirmButton: false,
            timer: 2500
        })
    
        Swal.fire({
          title: `¿Deseas eliminar la categoría ${catg.name} ?`,
          showCancelButton: true,
          confirmButtonText: `Sí`,
          cancelButtonText: `No`,
        }).then((result) => {
          if (result.isConfirmed) {
            deleteCategory(catg._id)
              .then(() => {
                setUpdate(true)
                message('success', `Categoría ${catg.name} eliminada con éxito.`)
              })
              .catch(() => message('danger', `Ha ocurrido un error`))
          }
        })
      }
    
    

    return (
        <>
        {isLoading
        ? <Loading/>
        :
            <RowCategory>

                
                    {categories.map((catg, i) => (
                        <CardCategory key={i} >
                        <Card.Body >
                        <Card.Title>{catg.name}</Card.Title>                     
                        
                        </Card.Body> 
                        <Card.Footer>
                        <Actions><BiTrash className="icon" onClick={() => removeCategory(catg)} /><BiEdit className="icon" onClick={() =>props.updateCategory(catg)}/></Actions>
                        </Card.Footer>
                        </CardCategory>

                    ))}
                                      
                
               
            </RowCategory>


                    }


        </>
    )
}



export default CategoriesList

const CardCategory = styled(Card)`
//width: 25rem;
//margin-bottom: 1.5rem;

.card-body{
    
    padding:0.75rem;
    display: flex;
    align-items: center;
    flex-wrap: wrap;
}

.card-title{
    margin: 0px;
    text-transform: uppercase;
}

.card-footer {
    padding: 0.5rem;
    text-align: right;
}

.right{
    text-align: right;
}
`
const RowCategory = styled.div`
margin-top: 1rem;
display: grid;
grid-gap: 1rem;
grid-template-columns: repeat(auto-fill,minmax(240px,1fr));

`
const Actions = styled.div`
display: block;
color: #6c757d;
.icon {
    font-size: 1.25em;
    cursor: pointer;
    margin-left: 0.5em;
}

`