import React from 'react'
import { Route } from 'react-router-dom'

import Layout from '../../components/layout/admin'

import Banner from './banner'
import Categories from './categories'
import Dashboard from './dashboard'
import Products from './products'


export default (props) => {
    return (
        <Layout>
            <Route exact basename={props.match.path} path={props.match.path + '/'} component={Dashboard}/>
            <Route exact basename={props.match.path} path={props.match.path + '/banner'} component={Banner}/>
            <Route exact basename={props.match.path} path={props.match.path + '/categorias'} component={Categories}/>
            <Route exact basename={props.match.path} path={props.match.path + '/productos'} component={Products}/>            
        </Layout>

    )
}