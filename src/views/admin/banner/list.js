import React, { useState, useEffect } from 'react'
import { Card } from 'react-bootstrap'
import styled from 'styled-components'
import { listBanner, deleteBanner } from '../../../services/admin'
import Loading from '../../../components/loading'

import { BiTrash, BiEdit } from 'react-icons/bi'
import Swal from 'sweetalert2'

const BannersList = (props) => {
    const [banner, setBanner] = useState([])
    const [update, setUpdate] = useState(false)
    const [isLoading, setIsLoading] = useState(false)
       

    useEffect(() => {
        setUpdate(false)
        let get = async () => { 
            setIsLoading(true)
            const bnr = await listBanner() 
            setBanner(bnr.data)
            setIsLoading(false)
        }        
            get()       
        
        return () => get = () => {
            //cleanup
        }
    }, [update])

    const removeBanner = async (catg) => {
        const message = (type, message) => Swal.fire({
            toast: true,
            position: 'bottom',
            width: '20rem',
            icon: type || '',
            title: message || '',
            showConfirmButton: false,
            timer: 2500
        })
    
        Swal.fire({
          title: `¿Deseas eliminar el banner?`,
          showCancelButton: true,
          confirmButtonText: `SÍ`,
          cancelButtonText: `NO`,
        }).then((result) => {
          if (result.isConfirmed) {
            deleteBanner(catg._id)
              .then(() => {
                setUpdate(true)
                message('success', `Banner eliminado con éxito.`)
              })
              .catch(() => message('danger', `Ha ocurrido un error`))
          }
        })
      }
    
      const sortBanner = banner.sort(function (a, b) {

        if (a.status> b.status) {
          return -1
        }
        if (a.status < b.status) {
          return 1
        }
    
        return 0
    
    
      })
    


    return (
        <>
        {isLoading
        ? <Loading/>
        : <RowBanner>                
            {sortBanner.map((bnr, i) => (
                    <CardBanner key={i} >
                        <Card.Img variant="top" src={bnr.photo} className={bnr.status === true ? "" : "img-inactive"} />
                    <Card.Body >
                    <Card.Title>{bnr.title}</Card.Title> 
                    <Card.Text>{bnr.description}</Card.Text>      
                                   
                   
                   </Card.Body> 
                    <Card.Footer>
                    <Actions><BiTrash className="icon" onClick={() => removeBanner(bnr)} /><BiEdit className="icon" onClick={() =>props.updateBanner(bnr)}/></Actions>
                    </Card.Footer>
                    </CardBanner>
                ))}
            </RowBanner>
        }
        </>
    )
}



export default BannersList

const CardBanner = styled(Card)`
//width: 25rem;
//margin-bottom: 1.5rem;

.card-body{
    
    padding:0.75rem;
   // display: flex;
    align-items: center;
    //flex-wrap: wrap;
}


.card-title{
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    margin: 0px;
    text-transform: uppercase;
    
}

.card-text{
    //white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    margin-top: 0.5rem;
    //text-transform: uppercase;
    
}

.card-img-top{
    max-height: 175px;
    
}

.img-inactive{
    filter: grayscale(100%);
    opacity: 60%;

}
.card-footer {
    padding: 0.5rem;
    text-align: right;
}

.right{
    text-align: right;
}
`
const RowBanner = styled.div`
margin-top: 1rem;
display: grid;
grid-gap: 1rem;
grid-template-columns: repeat(auto-fill,minmax(240px,1fr));

`
const Actions = styled.div`
display: block;
color: #6c757d;
.icon {
    font-size: 1.25em;
    cursor: pointer;
    margin-left: 0.5em;
}

`