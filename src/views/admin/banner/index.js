import React, { useState } from 'react'
import TitlePage from '../../../components/layout/admin/title'
import BannersList from './list'
import BannersForm from './form'

import { Button, Container } from 'react-bootstrap'
import { IoMdAddCircleOutline, IoIosList } from 'react-icons/io'
import styled from 'styled-components'


const Banner = () => {

    const [isForm, setIsForm] = useState(false)
    const [updateList, setUpdateList] = useState({})   
 

    const updateBanner = (bnr) => {        
        setIsForm(true)
        setUpdateList(bnr)    
    }

    const newBanner = () => {
        setUpdateList(false)
        setIsForm(!isForm)
    }


        
    return (
        <>
        <Container fluid>
            <TitlePage title="BANNER"/>
            
                <ButtonStyled onClick={() => newBanner()}>
                    {isForm 
                        ? <><IoIosList className="icon"/> LISTA </>
                        : <><IoMdAddCircleOutline className="icon"/> NUEVO </>
                    }
                </ButtonStyled>
                    {isForm 
                        ? <BannersForm update={updateList} />
                        : <BannersList updateBanner={updateBanner}/>
                    }                             
            
        </Container>
                 
           
        </>
    )
}

export default Banner

const ButtonStyled = styled(Button)`
padding: 0.5rem;
.icon {
    font-size: 1.3em;
    margin-right: 0.3rem;
    margin-top: -0.2rem;
    
}
`