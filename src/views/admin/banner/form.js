import React, { useState } from 'react'
import { addBanner, updateBanner } from '../../../services/admin'
import Swal from 'sweetalert2'

import { Row, Col, FormControl, Form, Card, Button, Spinner, Container, ButtonGroup, ToggleButton, Image } from 'react-bootstrap'
import { FaCheck, FaTimes } from "react-icons/fa"
import styled from 'styled-components'

const FormBanner = (props) => {

  const [form, setForm] = useState({
    ...props.update,
    //category: props.update?.category?._id || undefined
  })

  
  const [updatePhoto, setUpdatePhoto] = useState(false)
  const [loading, setLoading] = useState(false)

 
  const handleChange = (attr) => {
    const { name, value, checked } = attr.target
    const isCheck = name === "status" || name === "featured"
    if (name === "photo") {
      setForm({
        ...form,
        [name]: value,
        photo: attr.target.files[0],
      })
    } else {
      setForm({
        ...form,
        [name]: isCheck ? checked : value,
      })
    }
    return
  }

  const isUpdate = Object.keys(props.update).length > 0

  const typeReq = (data) => isUpdate ? updateBanner(props.update._id, data) : addBanner(data)

  const isFormValid = () =>
   
    form.title
    && form.description
    && form.photo

  const submitBanner = async () => {
    const message = (type, message) =>
      Swal.fire({
        icon: type || "success",
        title: message,
        showConfirmButton: false,
        timer: 2500,
      })

    setLoading(true)
    

    let data = new FormData()
    Object.keys(form)
            .forEach(key => data.append(key, form[key]))
            //Trecho de código dedicado ao colega 
            if (typeof form.photo === "string") {                
                data.delete('photo')
            //Gilson Conceição :)
            }  
    

    const config = {

      headers: {
        "Content-type": "multipart/form-data",
      },
    }

    typeReq(data, config)
      .then((res) => {
        setLoading(false)
        clearForm()
        message("success", `Banner ${isUpdate ? "actualizado" : "creado"} con éxito`)

      })
      .catch((err) => message("error", `Error al registrar el banner.`))
    setLoading(false)
  }

  const removePhoto = () => {
    setUpdatePhoto(true)
    setForm({
      ...form,
      photo: "",
    })
  }

  const clearForm = () => {
    setUpdatePhoto(true);
    setForm({
      category: "",
      status: false,
      highlight: false,
      photo: "",
    });
  };


  return (
    <>
      <Container fluid>
        <RowForm>
          <Col sm={4} md={4} xl={4}>
            <Card.Title className="mb-4">{isUpdate ? "EDITAR BANNER" : "NUEVO BANNER"}</Card.Title>
            <Form.Group>
              <ButtonGroup toggle className="mb-1 mr-4">
                <ToggleButton
                  type="checkbox"
                  variant={Boolean(form.status) ? "success" : "danger"}
                  name="status"
                  onChange={handleChange}
                  checked={Boolean(form.status)}
                >
                  {Boolean(form.status) ? <FaCheck /> : <FaTimes />} Mostrar
          </ToggleButton>
              </ButtonGroup>             
            </Form.Group>
            <Form.Group>
              {isUpdate && !updatePhoto ? (
                <>
                  <Image src={form.photo} thumbnail />
                  <ActionPhoto onClick={removePhoto}><FaTimes className="icon" />Eliminar Foto</ActionPhoto>
                </>
              ) : (
                  <input name="photo" type="file" onChange={handleChange} className="file" />
                )}
            </Form.Group>
          </Col>
          <Col sm={4} md={4} xl={4}>            
            <FormControl type="text" name="title" className="input mb-3" placeholder="Nombre" onChange={handleChange} value={form.title || ""} />
            <FormControl type="text" name="description" className="input mb-3" placeholder="Descripción completa" onChange={handleChange} value={form.description || ""} as="textarea" rows={3} />
            <Action>
              <Button className="btn" variant="primary" onClick={submitBanner} disabled={!isFormValid()}>
                {loading ? (
                  <Spinner animation="border" size="sm" />
                ) : "ENVIAR"}
              </Button >
            </Action>
            
            
          </Col>
          <Col sm={4} md={4} xl={4}>
            
           
          </Col>
        </RowForm>
      </Container>
    </>
  )
}

export default FormBanner

const Action = styled.div`
display: flex;
flex-direction: column;
justify-content: flex-end;

`
const RowForm = styled(Row)`
margin-top: 1rem;
background: #FFF;
border: 1px solid rgba(0,0,0,.125);
padding-top: 2rem;
padding-bottom: 2rem;
.file{
  color: transparent;
}
`
const ActionPhoto = styled.div`
  display: flex;
  justify-content: flex-end;
  cursor: pointer;
  color: #212529;
    &:hover {
      color: #dc3545;
    }
    .icon {
      margin-top: 0.3rem;
      margin-right: 0.3rem;
    }
  
`

