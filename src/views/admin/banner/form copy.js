import React, { useState, useEffect } from 'react'
import { addProduct, updateProduct, listCategory } from '../../../services/admin'
import Swal from 'sweetalert2'

import { Row, Col, FormControl, Form, Card, Button, Spinner, InputGroup, Container, ButtonGroup, ToggleButton, Image } from 'react-bootstrap'
import { FaCheck, FaTimes } from "react-icons/fa"
import styled from 'styled-components'

const FormProducts = (props) => {

  const [form, setForm] = useState({
    ...props.update,
    //category: props.update?.category?._id || undefined
  })

  const [categories, setCategories] = useState([])
  const [updatePhoto, setUpdatePhoto] = useState(false)
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    let get = async () => {
      const category = await listCategory()
      setCategories(category.data)
    }
    get()

    return () => (get = () => {
      //cleanup
    })
  }, [])



  const handleChange = (attr) => {
    const { name, value, checked } = attr.target
    const isCheck = name === "status" || name === "featured"
    if (name === "photo") {
      setForm({
        ...form,
        [name]: value,
        photo: attr.target.files[0],
      })
    } else {
      setForm({
        ...form,
        [name]: isCheck ? checked : value,
      })
    }
    return
  }

  const isUpdate = Object.keys(props.update).length > 0

  const typeReq = (data) => isUpdate ? updateProduct(props.update._id, data) : addProduct(data)

  const isFormValid = () =>
    form.category
    && form.title
    && form.price
    && form.photo

  const submitProduct = async () => {
    const message = (type, message) =>
      Swal.fire({
        icon: type || "success",
        title: message,
        showConfirmButton: false,
        timer: 2500,
      })

    setLoading(true)
    

    let data = new FormData()
    Object.keys(form)
            .forEach(key => data.append(key, form[key]))
            //Trecho de código dedicado ao colega 
            if (typeof form.photo === "string") {                
                data.delete('photo')
            //Gilson Conceição :)
            }  
    

    const config = {

      headers: {
        "Content-type": "multipart/form-data",
      },
    }

    typeReq(data, config)
      .then((res) => {
        setLoading(false)
        clearForm()
        message("success", `Producto ${isUpdate ? "actualizado" : "registrado"} con éxito`)

      })
      .catch((err) => message("error", `Error al registrar el producto.`))
    setLoading(false)
  }

  const removePhoto = () => {
    setUpdatePhoto(true)
    setForm({
      ...form,
      photo: "",
    })
  }

  const clearForm = () => {
    setUpdatePhoto(true);
    setForm({
      category: "",
      status: false,
      highlight: false,
      photo: "",
    });
  };


  return (
    <>
      <Container fluid>
        <RowForm>
          <Col sm={4} md={4} xl={4}>
            <Card.Title className="mb-4">{isUpdate ? "EDITAR PRODUCTO" : "NUEVO PRODUCTO"}</Card.Title>
            <Form.Group>
              <ButtonGroup toggle className="mb-1 mr-4">
                <ToggleButton
                  type="checkbox"
                  variant={Boolean(form.status) ? "success" : "danger"}
                  name="status"
                  onChange={handleChange}
                  checked={Boolean(form.status)}
                >
                  {Boolean(form.status) ? <FaCheck /> : <FaTimes />} Mostrar
          </ToggleButton>
              </ButtonGroup>
              <ButtonGroup toggle className="mb-1">
                <ToggleButton
                  type="checkbox"
                  variant={Boolean(form.featured) ? "success" : "danger"}
                  name="featured"
                  onChange={handleChange}
                  checked={Boolean(form.featured)}
                >
                  {Boolean(form.featured) ? <FaCheck /> : <FaTimes />}
            Destaque
          </ToggleButton>
              </ButtonGroup>
            </Form.Group>
            <Form.Group>
              {isUpdate && !updatePhoto ? (
                <>
                  <Image src={form.photo} thumbnail />
                  <ActionPhoto onClick={removePhoto}><FaTimes className="icon" />Eliminar Foto</ActionPhoto>
                </>
              ) : (
                  <input name="photo" type="file" onChange={handleChange} className="file" />
                )}
            </Form.Group>
          </Col>
          <Col sm={4} md={4} xl={4}>
            <Form.Group controlId="exampleForm.ControlSelect1" >
              <Form.Control as="select" custom name="category" onChange={handleChange} value={form.category}>
                <option>Categoría</option>
                {categories.map((catg, i) => (
                  <option key={i} value={catg._id}>
                    {catg.name}
                  </option>
                )
                )}
              </Form.Control>
            </Form.Group>
            <FormControl type="text" name="title" className="input mb-3" placeholder="Nombre" onChange={handleChange} value={form.title || ""} />
            <InputGroup className="mb-3">
              <FormControl type="number" min="0" max="100" name="price" placeholder="Precio" step=".01" maxLength="4" onChange={handleChange} value={form.price || ""} />
              <InputGroup.Append>
                <InputGroup.Text>€</InputGroup.Text>
              </InputGroup.Append>
            </InputGroup>
            <InputGroup className="mb-3">
              <FormControl type="number" min="0" max="100" name="discount_price" placeholder="Precio con descuento" step=".01" maxLength="4" onChange={handleChange} value={form.discount_price || ""} />
              <InputGroup.Append>
                <InputGroup.Text>€</InputGroup.Text>
              </InputGroup.Append>
            </InputGroup>
          </Col>
          <Col sm={4} md={4} xl={4}>
            <FormControl type="text" name="description" className="input mb-3" placeholder="Descripción" onChange={handleChange} value={form.description || ""} />
            <FormControl type="text" name="complete_description" className="input mb-3" placeholder="Descripción completa" onChange={handleChange} value={form.complete_description || ""} as="textarea" rows={3} />
            <Action>
              <Button className="btn" variant="primary" onClick={submitProduct} disabled={!isFormValid()}>
                {loading ? (
                  <Spinner animation="border" size="sm" />
                ) : "ENVIAR"}
              </Button >
            </Action>
          </Col>
        </RowForm>
      </Container>
    </>
  )
}

export default FormProducts

const Action = styled.div`
display: flex;
flex-direction: column;
justify-content: flex-end;

`
const RowForm = styled(Row)`
margin-top: 1rem;
background: #FFF;
border: 1px solid rgba(0,0,0,.125);
padding-top: 2rem;
padding-bottom: 2rem;
.file{
  color: transparent;
}
`
const ActionPhoto = styled.div`
  display: flex;
  justify-content: flex-end;
  cursor: pointer;
  color: #212529;
    &:hover {
      color: #dc3545;
    }
    .icon {
      margin-top: 0.3rem;
      margin-right: 0.3rem;
    }
  
`

